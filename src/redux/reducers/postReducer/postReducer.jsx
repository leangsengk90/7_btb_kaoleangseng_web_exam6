import { GET_POSTS } from "../../actions/postAction/postActionType"

const defaultState = {
    data: []
}

export const postReducer = (state = defaultState, action) => {
    switch(action.type){
        case GET_POSTS:
            return{
                ...state,
                data: action.data
            }
        default:
            return state
    }
}