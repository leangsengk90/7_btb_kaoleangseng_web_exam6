import Axios from 'axios'
import { baseURL } from '../../../config/API';
import { GET_POSTS } from './postActionType';

export const getPosts = () => {
    const innerGetPosts = async(dispatch) => {
        const result = await Axios.get(`${baseURL}/posts`);
        console.log(result);
        dispatch({
            type: GET_POSTS,
            data: result.data
        })
    }
    return innerGetPosts
}