
export const GET_POSTS = "GET_POSTS"
export const POST_POSTS = "POST_POSTS"
export const UPDATE_POST = "UPDATE_POST"
export const DELETE_POST = "DELETE_POST"