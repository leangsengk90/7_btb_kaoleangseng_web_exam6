import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {getPosts} from '../../redux/actions/postAction/postAction'

const ListPosts = (props) => {
  return(
    <div className="ListPostsWrapper">
      <button onClick={()=>{
        props.getPosts()
      }}>Get Posts Here</button>
      {
        props.data.map((item,index)=>{
        return <p key={index}>{item.title}</p>
        })
      }
    </div>
  );
}

const mapStateToProps = state => {
  return {
    data: state.postReducer.data
  }
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getPosts,
  },dispatch)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListPosts);
