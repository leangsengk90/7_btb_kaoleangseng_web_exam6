import React from 'react';
import logo from './logo.svg';
import './App.css';
import ListPosts from './components/ListPosts/ListPosts';
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from './components/Main/Main';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Contact from './components/Main/Contact';

function App() {
  return (
    <div className="App">
      <Router>
        <Main />
        <Switch>
          <Route path="/home" component={ListPosts} />
          <Route path="/contact" component={Contact} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
